/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegopreguntas;

/**
 *
 * @author idark
 */
public class Paralelo {
    String materia;
    String termino;
    int numParalelo;

    public Paralelo(String materia, String termino, int numParalelo) {
        this.materia = materia;
        this.termino = termino;
        this.numParalelo = numParalelo;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getTermino() {
        return termino;
    }

    public void setTermino(String termino) {
        this.termino = termino;
    }

    public int getNumParalelo() {
        return numParalelo;
    }

    public void setNumParalelo(int numParalelo) {
        this.numParalelo = numParalelo;
    }
    
    @Override
    //public String toString() {return nombre+" "+codigo+" "+nivel;}
    public String toString() {return materia+" "+termino+" "+numParalelo;}
    
}
