
package juegopreguntas;

/**
 *
 * @author idark
 */
public class Materia {
    private String nombre;
    private String codigo;
    private int nivel;
    private int estado; //define si la materia esta activada o desactivada, 1 activado y 0 desactivado

    public Materia(String nombre, String codigo, int nivel) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.nivel = nivel;
        this.estado = 1;//1 esta activado 0 esta desactivado
    }

    public int getNivel() {return nivel;}
    public void setNivel(int nivel) {this.nivel = nivel;}
    
    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getCodigo() {return codigo;}
    public void setCodigo(String codigo) {this.codigo = codigo;}
    
    public int getEstado() {return estado;}
    public void setEstado(int estado) {this.estado = estado;}
    
    @Override
    //public String toString() {return nombre+" "+codigo+" "+nivel;}
    public String toString() {return nombre+" "+codigo+" "+nivel+" "+estado;}
}
