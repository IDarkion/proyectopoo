
package juegopreguntas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author idark
 */
public class JuegoPreguntas {
    int opc;
    String materiaEscogida;
    String terminoEscogido;
    ArrayList<Termino> terminos = new ArrayList<>();
    ArrayList<Materia> materias = new ArrayList<>();
    ArrayList<Paralelo> paralelos = new ArrayList<>();
    ArrayList<Pregunta> preguntas = new ArrayList<>();
    Scanner input = new Scanner(System.in);
    
    
    public void juegoPreguntas(){}
    
    public static void main (String args[]){
        JuegoPreguntas juego = new JuegoPreguntas();
        juego.cargarPreguntas();
        juego.menuInicial();
    }

    public void menuInicial(){
        materias.add(new Materia("POO","123",3));
        while(true){
        limpiar();
        print("MENÚ PRINCIPAL "
        + "\n1. Configuraciones"
        + "\n2. Nuevo Juego"
        + "\n3. Reporte"
        + "\n4. Salir"
        + "\nopc >> ");
        opc = input.nextInt();
        if(opc == 1){configuraciones();}
        //else if(opc == 2){nuevoJuego();}
        //else if(opc == 3){reporte();}
        else if(opc == 4){break;}
        else{leeBien();}
        }
    } 
       
    public void configuraciones(){
        while(true){
        limpiar();
        System.out.print("Bienvenido a las configuraciones del juego"
        + "\n1.Administrar términos académicos"
        + "\n2.Administrar materias y paralelos"
        + "\n3.Administrar preguntas"
        + "\n4.Salir"
        + "\nopc >> ");
        opc = input.nextInt();
        if (opc==1){adminTerminos();}
        else if (opc==2){adminMaterias();}
        else if (opc==3){adminPreguntas();}
        else if (opc==4){break;}
        else {leeBien();}} 
    }
    
    public void adminTerminos(){
        while(true){
        limpiar();
        System.out.println("TERMINOS ACADEMICOS:");
        System.out.println(terminos);
        System.out.print("1.Ingresar termino"
        + "\n2.Eliminar termino"
        + "\n3.Configurar termino para juego"
        + "\n4.Salir"
        + "\nopc >> ");
        opc = input.nextInt();
        limpiar();
        if (opc==1){
            System.out.println("Ingresar un término nuevo");
            System.out.print("Año >> ");
            int anio = input.nextInt();
            System.out.print("Número del término >> ");
            int numero = input.nextInt();
            terminos.add(new Termino(anio,numero));
            delay(1000);}
        else if (opc==2){        
            System.out.println("Eliminar término");
            System.out.println("Escribe el término que quieres eliminar:");   
            input.nextLine();
            String termino = input.nextLine();                
            for(int i=0;i<terminos.size();i++){
                System.out.println(terminos.get(i).getTermino());
                if (termino.equals(terminos.get(i).getTermino())){
                    terminos.remove(i);
                    System.out.println("Termino Elimnado de manera Correcta");}}
            delay(2000);}
        else if (opc==3){
            System.out.println("Configurar término para juego");
            System.out.println("Ingrese termino y estudiante para el nuevo juego:");
            System.out.println("Escribe el término que escogeras para el juego:");
            input.nextLine();
            terminoEscogido=input.nextLine();
            delay(2000);}
        else if (opc==4){break;}
        else{leeBien();}
        }}
    
    public void adminMaterias(){
        String codigo;
        String nombre;
        while(true){
        limpiar();
        System.out.println("Materias y Paralelos");
        System.out.println(materias);
        System.out.print("1. Ingresar materia"
                + "\n2. Editar materia"
                + "\n3. Desactivar materia"
                + "\n4. Agregar paralelo"
                + "\n5. Eliminar paralelo"
                + "\n6. Salir" 
                + "\nopc >> "); 
        int opc = input.nextInt();
        limpiar();
        if(opc==1){
            System.out.println("Ingresar materia");
            System.out.println("Nombre de la materia: ");
            input.nextLine();
            nombre = input.nextLine();
            System.out.println("Código de la materia: ");
            codigo = input.nextLine();
            System.out.println("Cantidad de niveles: ");
            int nivel = input.nextInt();
            materias.add(new Materia(nombre,codigo,nivel));
            limpiar();
            System.out.println("la materia: "+nombre+"-"+codigo+ " se agrego correctamente");
            delay(3000);}
        else  if(opc==2){
            System.out.println("Editar materia");
            System.out.println("Código de la materia: ");
            input.nextLine();
            codigo = input.nextLine();
            System.out.println("Nuevo nivel al que quieres cambiar: ");
            int nivel = input.nextInt();
            System.out.println("nuevo nombre al que quieres cambiar; ");
            input.nextLine();
            nombre = input.nextLine();
            for(int i=0;i<materias.size();i++){
               if (codigo.equals(materias.get(i).getCodigo())){
                   materias.set(i,new Materia(nombre,codigo,nivel));
                   System.out.println("la materia ha sido actualizada");}}}
        else if(opc==3){
            println("Desactivar materia");
            println("Codigo de la materia que quieres desactivar");
            input.nextLine();
            codigo = input.nextLine();
            println("quieres en verdad desactivar esta materia?"
            +"1. SI"
            +"2. No");
            opc = input.nextInt();input.nextLine();
            if(opc==1){
            for(int i=0;i<materias.size();i++){
                if (codigo.equals(materias.get(i).getCodigo())){
                   materias.get(i).setEstado(0);}}}}
        else if(opc==4){
            println("Agregar Paralelo");
            println("Nombre de la materia: ");
            input.nextLine();
            String materia = input.nextLine();
            println("Termino academico: ");
            String termino = input.nextLine(); //Termino academico
            println("Numero de paralelo");
            int numParalelo = input.nextInt();input.nextLine(); //Numero del paralelo
            paralelos.add(new Paralelo(materia,termino,numParalelo));}
        else if(opc==5){
            System.out.println(paralelos);
            println("Eliminar paralelo");
            System.out.println("Escribe el paralelo que quieres eliminar:");   
            input.nextLine();
            int numParalelo = input.nextInt();                
            for(int i=0;i<paralelos.size();i++){
                if (numParalelo == paralelos.get(i).getNumParalelo()){
                    paralelos.remove(i);
                    System.out.println("Paralelo elimnado de manera Correcta");}}
            delay(2000);}
        else if(opc==6){break;}
        else{leeBien();}}   
    }
  
    public void adminPreguntas(){
        while(true){
        String codigo;
        String enunciado;
        String nivel;
        String respCorrecta;
        String resp1;
        String resp2;
        String resp3;
        System.out.println("Escoja una opcion: "
        + "\n1.Visualizar preguntas"
        + "\n2.Agregar pregunta"
        + "\n3.Eliminar paralelo"
        + "\n4.Salir");
        opc = input.nextInt();input.nextLine();
        if(opc==1){
            System.out.println("Código de la materia: ");
            codigo = input.nextLine();
            System.out.println(preguntas);}
        else if(opc==2){
            System.out.println("Código de la materia: ");
            codigo = input.nextLine();
            if(materias.get(0).getEstado()==1){//Si la materia esta activada
            System.out.println("Para esta materia se ha establecido un nivel de: "+materias.get(0).getNivel());
            System.out.println("Enunciado de la pregunta: ");
            enunciado = input.nextLine();
            System.out.println("nivel de la pregunta: ");
            nivel = input.nextLine();
            System.out.println("Respuesta correcta: ");
            respCorrecta = input.nextLine();
            System.out.println("resp1: ");
            resp1 = input.nextLine();
            System.out.println("resp2: ");
            resp2 = input.nextLine();
            System.out.println("resp3: ");
            resp3 = input.nextLine();
            preguntas.add(new Pregunta(enunciado, nivel, respCorrecta, resp1, resp2, resp3));
                guardarPregunta(enunciado, nivel, respCorrecta, resp1, resp2, resp3);
            
            }
            
        }
        else if(opc==3){}
        else if(opc==4){break;}
        else{leeBien();}
        
        }}


//        
//    public void adminPreguntas(){
//        int opc=0;
//        String codigoMateria;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Escoja una opcion: \n1.Visualizar preguntas\n2.Agregar pregunta\n3.Eliminar paralelo\n4.Salir");
//        opc = sc.nextInt();
//
//        System.out.println("Bienvenido a las configuraciones del juego\n Se le mostrara una lista con terminos por defecto observela ytome las debidas acciones "
//                + "");
//        Scanner entrada = new Scanner(System.in);
//        System.out.println(terminos);
//        System.out.println("Escoja una opcion porfavor:\n1.Ingresar termino\n2.Eliminar termino\n3.Configurar termino para juego");
//        for(Termino apuntador: terminos){
//            System.out.println(apuntador.toString());
//        }
//        switch(opc){
//            case 1:
//                System.out.println("1.Visualizar preguntas");
//                System.out.println("Código de la materia: ");
//                codigoMateria=sc.nextLine();
//                //Falta que muestre las preguntas
//                break;
//            case 2:
//                System.out.println("1.Agregar pregunta");
//                break;
//            case 3:
//                System.out.println("1.Eliminar paralelo");
//                break;
//            case 4:
//                configuraciones();
//                break;
//        }
//    }
//
//    public void nuevojuego(){
//        String Materia;
//        int Paralelo;
//        int Nivel;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Nuevo juego");
//        System.out.println("Nombre de la materia:");
//        Materia=sc.nextLine();
//        System.out.println("Número del paralelo: ");
//        Paralelo=sc.nextInt();
//        System.out.println("Número de niveles:");
//        Nivel=sc.nextInt();
//        System.out.println("Presione una tecla para empezar");
//        
//    }
//    
//    public void reporte(){
//        String Termino;
//        String codigoParalelo;
//        int Paralelo;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Término:");
//        Termino=sc.nextLine();
//        System.out.println("Código del paralelo:");
//        codigoParalelo=sc.nextLine();
//        System.out.println("Número del paralelo: ");
//        Paralelo=sc.nextInt();
//        
//    
    public void cargarPreguntas() {
        String fila;
        BufferedReader csvReader = null;
        try {
            String ruta = "src/archivos/preguntas.csv"; //ruta del archivo que se va a leer
            csvReader = new BufferedReader(new FileReader(ruta));    
            while ((fila = csvReader.readLine()) != null) { //iterar en el contenido del archivo
            String[] data = fila.split(";");
            preguntas.add(new Pregunta(data[1], data[0], data[2], data[3], data[4], data[5]));}} //crear objeto y agregar a lista
        catch(FileNotFoundException ex){Logger.getLogger(JuegoPreguntas.class.getName()).log(Level.SEVERE, null, ex);} 
        catch(IOException ex){Logger.getLogger(JuegoPreguntas.class.getName()).log(Level.SEVERE, null, ex);} 
        finally{
            try {
                csvReader.close();} 
            catch(IOException ex){
                Logger.getLogger(JuegoPreguntas.class.getName()).log(Level.SEVERE, null, ex);}}}
    
    public void guardarPregunta(String enunciado, String nivel, String respCorrecta, String resp1, String resp2, String resp3){
        FileWriter writer = null;
        try {
            String ruta = "src/archivos/preguntas.csv"; //ruta del archivo que se va a escribir
            writer = new FileWriter(ruta);
            for (int i=0;i<preguntas.size();i++) {
            writer.write(preguntas.get(i).getEnunciado()
                    +";"+preguntas.get(i).getNivel()
                    +";"+preguntas.get(i).getRespCorrecta()
                    +";"+preguntas.get(i).getResp1()
                    +";"+preguntas.get(i).getResp2()
                    +";"+preguntas.get(i).getResp3()
                    +System.lineSeparator());
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(JuegoPreguntas.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(JuegoPreguntas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void limpiar(){
        for(int i=0;i!=10;i++){System.out.println("");}
    }
    
    public static void delay(int milis){
        try {Thread.sleep(milis);} 
        catch (InterruptedException ex) {}
    }
   
    public static void leeBien(){
        limpiar();
        System.out.println("Ingrese un valor correcto");
        delay(2000);
        limpiar();
    }
    
    public static void print(String texto){
        System.out.print(texto);
    }
    
    public static void println(String texto){
        System.out.println(texto);
    }
}
