/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegopreguntas;
/**
 *
 * @author idark
 */
public class Pregunta {
    
    
    private String enunciado;
    private String nivel;
    private String respCorrecta;
    private String resp1;
    private String resp2;
    private String resp3;

    public Pregunta(String enunciado, String nivel, String respCorrecta, String resp1, String resp2, String resp3) {
        this.enunciado = enunciado;
        this.nivel = nivel;
        this.respCorrecta = respCorrecta;
        this.resp1 = resp1;
        this.resp2 = resp2;
        this.resp3 = resp3;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getRespCorrecta() {
        return respCorrecta;
    }

    public void setRespCorrecta(String respCorrecta) {
        this.respCorrecta = respCorrecta;
    }

    public String getResp1() {
        return resp1;
    }

    public void setResp1(String resp1) {
        this.resp1 = resp1;
    }

    public String getResp2() {
        return resp2;
    }

    public void setResp2(String resp2) {
        this.resp2 = resp2;
    }

    public String getResp3() {
        return resp3;
    }

    public void setResp3(String resp3) {
        this.resp3 = resp3;
    }

    
    
    @Override
    public String toString() {return enunciado+" "+nivel+" "+respCorrecta+" "+resp1+" "+resp2+" "+resp3;}
    
}
