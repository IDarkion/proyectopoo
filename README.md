# juego-preguntas

## juego de preguntas al estilo quien quiere ser millonario

Antecedentes La gamificación es una forma de incorporar estrategias de juego que pueden apoyar y motivar el trabajo de estudiantes y maestros en el salón. Para apoyar en la tarea de docencia de un profesor politécnico su grupo de trabajo desarrollará un juego de preguntas              y respuestas basándose en el juego Quién quiere ser millonario. En este juego un participante debe contestar correctamente una serie de preguntas que están agrupadas por nivel. Durante el juego, dispone de 3 comodines para ayudar a acertar la respuesta correcta. 

- 50/50: descarta dos de las respuestas incorrectas aleatoriamente 

- Consulta al compañero: El participante puede consultar la respuesta a un compañero del curso que ha elegido al inicio del juego 

- Consulta al salón: El participante puede consultar la respuesta a todos los compañeros del salón con el objetivo de seleccionar la que elija la mayoría.   

Especificaciones La aplicación permitirá al docente configurar las materias que dicta, los paralelos y los              estudiantes registrados en cada uno, así como establecer la cantidad de niveles y             preguntas. 
 
Al iniciar la aplicación se mostrará el siguiente menú: 1. Configuraciones 2. Nuevo juego 3. Reporte 4. Salir 
 
Opción 1: Configuraciones Se mostrará el siguiente menú 1. Administrar términos académicos 2. Administrar materias y paralelos 3. Administrar estudiantes 4. Administrar preguntas 5. Salir 
 
Detalle de opciones: 1. Administrar términos académicos 
 
Al ingresar a esta opción se visualizará un listado con los términos académicos. Además se podrán agregar, eliminar términos académicos. Un término académico está           formado por el año y el número.  Por ejemplo 2019-2 En esta opción el profesor podrá: 1.- Ingresar término  
2.- Eliminar término  3.- Configurar término para el juego 
 
1.1 Ingresar término En esta opción se solicitará el año y número de término. 
 
1.2 Eliminar término  En esta opción el usuario podrá eliminar alguno de los términos existentes. 
 
1.3 Configurar término para el juego En esta opción se selecciona el término del cual se van a seleccionar las materias y                estudiantes al crear un nuevo juego. 
 
2. Administrar materias y paralelos 
 
Al ingresar a esta opción se visualizará un listado con todas las materias y paralelos que el                 profesor haya dictado. Además, en esta sección se podrán agregar,editar, eliminar materias y paralelos.  En esta opción el profesor podrá: 1.- Ingresar materia 2.- Editar materia 3.- Desactivar materia 4.- Agregar paralelo 5.- Eliminar paralelo 
 
2.1 Ingresar materia Se solicita información básica de la materia como código y nombre. Además de la cantidad               de niveles en las que se van a agrupar las preguntas para el juego. 
 
2.2 Editar materia Se solicita el código de materia que se va a editar. Se puede modificar el nombre y la                  cantidad de niveles 
 
2.3 Desactivar materia Se solicita el código de materia que se va a desactivar. Se pregunta al usuario si está                 seguro que se desea desactivar. Las materias inactivas no se mostrarán en la             configuración de paralelos y preguntas 
 
2.4 Agregar paralelo Se solicita la materia, el término académico y el número del paralelo 
 
2.5 Eliminar paralelo Se muestran todos los paralelos existentes para que el usuario pueda seleccionar el que              desea eliminar 
 
3. Administrar estudiantes 
En esta sección se podrá visualizar el listado de estudiantes por término académico y              paralelo de materia.  Se solicitará el término académico, código de materia y paralelo. 
 
Para facilitar la administración de estudiantes, la información de ellos estará almacenada en             archivos de texto en un directorio determinado. El archivo deberá tener como nombre             codigomateria-paralelo-año-termino.csv Por ejemplo la lista de estudiantes del paralelo 2 de POO del 2019-2 estará en el archivo                 CCPG1005-2-2019-2.csv El archivo tendrá en cada línea la siguiente información del estudiante: matricula,nombre completo,email 
 
Ejemplos de cómo leer de archivos serán proporcionados por el profesor 
 
4. Administrar preguntas En esta sección va a poder visualizar, agregar, eliminar preguntas que se utilizan en el               juego. Una pregunta tiene el enunciado, el nivel y las posibles respuestas de las cuales               solo una es la correcta. El profesor podrá realizar lo siguiente: 1.- Visualizar preguntas 2.- Agregar pregunta 3.- Eliminar pregunta 
 
4.1 Visualizar preguntas Se solicitará al usuario que ingrese el código de la materia y a continuación se mostrará las                 preguntas ordenadas por nivel 
 
4.2 Agregar pregunta Se pedirá la siguiente información de la pregunta: ● Código de materia (validar que esté activa). Si existe debe mostrar cuántos nivel se              han establecido para esa materia ● Enunciado de Pregunta ● Nivel (minimo 1 y max dependiendo de la cantidad de niveles de la materia) ● Respuesta Correcta ● Posible Respuesta 1 ● Posible Respuesta 2 ● Posible Respuesta 3 
 
Las preguntas se almacenarán en un archivo csv que tiene el nombre el código de la                materia. 
 
Opción 2: Nuevo Juego En esta opción se creará un nuevo juego en el término que ha sido establecido. Se solicitará lo siguiente: ● La materia y el paralelo. La materia servirá para saber de dónde elegir las              preguntas.  El paralelo servirá para seleccionar al estudiante que participará 
● Número de preguntas por nivel. Luego de ingresar este valor se debe validar que              exista esta cantidad de preguntas en cada nivel. ● Participante. Puede ingresar el número de matrícula del estudiante o seleccionar           aleatorio del listado. 
 
Con esta información se inicia el juego. Mostrar un mensaje que diga: Presione una letra               para continuar  
 
Las preguntas se muestran junto con las opciones una por una. Las opciones serán A, B, C                 y D. Si en alguna pregunta se desea usar un comodín. Se deberá ingresar * para mostrar                 los comodines disponibles. Al seleccionar un comodín se debe volver a la pregunta para              hacer la elección de la respuesta correcta. El juego termina cuando el participante haya respondido de manera incorrecta alguna            pregunta o haya respondido correctamente a todas las preguntas seleccionadas para el            juego. Al final del juego si el estudiante ha superado un nivel o completado todas las preguntas se                 va a solicitar al profesor que ingrese el premio que el estudiante ha ganado. Luego de esto                 se enviará un correo al estudiante con esta información. 
 
Opción 3: Reporte En esta opción se podrá visualizar los juegos realizados ordenados por fecha. Se solicitará el término académico, código de materia y paralelo La información a mostrar será Fecha del juego Participante Nivel máximo alcanzado Premio 
 
 
Criterios de Evaluación Documentación (10 %) ● El programa fuente deberá presentar un código documentado internamente usando          Java doc (un correcto uso de comentarios). ● Diagrama de clases final  
 
Funcionalidad (90%) ● Se valorará la apariencia del proyecto, sin que esto implique que el estudiante deba              utilizar recursos adicionales a los aprendidos en clase. (El proyecto es en consola) ● Se verificará el cumplimiento de toda la funcionalidad requerida, así como las            correctas validaciones de los datos. ● El proyecto debe ser debidamente probado antes de presentarlo al profesor. Por lo             tanto se espera que el programa no se caiga al ejecutarlo, ni tenga un              comportamiento no esperado (Sólo se revisará funcionalidad sin errores) ● Cada estudiante deberá registrar al menos 4 commits en el repositorio de bitbucket             con cambios significativos incluyendo comentarios. 
 
  Tener presente en el desarrollo de su solución: Abstracción y Uso de Objetos 
● Creación de Clases pertinentes con sus propiedades y métodos ● Correcta interacción de objetos. Que los objetos se comuniquen entre sí y no solo              sean llamados todos en el main. 
 
  Modularidad y Encapsulamiento ● Dividir el problema usando los métodos correspondientes. No escribir bloques          inmensos de código. ● Encapsular correctamente el proyecto. Crear paquetes donde se agrupen clases que           se relacionen. ● No olvidar que para acceder a las propiedades de las clases se debe proveer los               métodos get y set.   Penalidades 5 por ciento menos por no seguir buenas prácticas de programación (convención para             nombre de clases, nombre de atributos y métodos, uso de constantes cuando es necesario,              o algún fallo grave que se detecte en la forma de programar). 
 
Componentes 
 
Proyecto 100 Factor sustentación individual 1 Factor uso git 1 Factor trabajo en grupo 1 
 
Nota Final = Proyecto x Factor sustentación x Factor uso git x Factor trabajo en grupo. 
 
Fecha de entrega Los entregables del proyecto deberán subirse a Sidweb hasta el​viernes22denoviembre              23:59 El dìa ​lunes 2 de diciembre ​en el horario de clases se realizará la sustentación del                proyecto.
