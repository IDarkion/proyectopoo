
package juegopreguntas;

/**
 *
 * @author idark
 */
public class Termino {
    private int anio;
    private int numero;
    private String termino;

    public Termino(int anio, int numero) {
        this.anio = anio;
        this.numero = numero;
        termino = anio+"-"+numero;
    }
    
    public Termino(String termino){
        this.termino = termino;
    }

    public int getAnio() {return anio;}
    public void setAnio(int anio) {this.anio = anio;}
    
    public int getNumero() {return numero;}
    public void setNumero(int numero) {this.numero = numero;}
    
    public String getTermino() {return termino;}
    public void setTermino(String termino) {this.termino = termino;}
 
    @Override
    public String toString() {return "termino: "+termino;}

}
